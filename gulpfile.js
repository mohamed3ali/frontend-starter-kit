var gulp = require('gulp'),
    jade = require('gulp-jade'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    uglify = require('gulp-uglify'),
    streamify = require('gulp-streamify'),
    sass = require('gulp-sass'),
    connect = require('gulp-connect'),
    autoprefixer = require('gulp-autoprefixer'),
    gulpif = require('gulp-if');

var env = process.env.NODE_ENV || 'development'
var outputDir = 'builds/development';

gulp.task('jade', function(){
    return gulp.src('src/templates/**/*.jade')
        .pipe(jade())
        .pipe(gulp.dest(outputDir))
        .pipe(connect.reload());
    });

gulp.task('js', function(){
    return browserify('src/js/main')
        //.bundle({ debug: true })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(streamify(uglify()))
        .pipe(gulp.dest(outputDir + '/js'))
        .pipe(connect.reload());
    });

gulp.task('sass', function(){
    var config = {};

    //if (env === 'development'){
        config.sourceComments = 'map';
    //}

    //if (env === 'production'){
        config.outputStyle = 'compressed';
    //}

    return gulp.src('src/sass/main.scss')
        .pipe(autoprefixer())
        .pipe(sass(config))
        .pipe(gulp.dest(outputDir + '/css'))
        .pipe(connect.reload());
    });

gulp.task('watch', function() {
    gulp.watch('src/templates/**/*.jade', ['jade']);
    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/sass/**/*.scss', ['sass']);
    });

gulp.task('connect', function() {
  connect.server({
    root: outputDir,
    livereload: true
  });
});

gulp.task('default', ['js', 'jade', 'sass', 'watch', 'connect']);